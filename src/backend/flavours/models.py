from __future__ import annotations

import typing

from django.db import models

if typing.TYPE_CHECKING:
    from django.http import Http404


class FlavourManager(models.Manager):
    def exist_by_name(
        self, *, name: typing.Annotated[str, "name of flavour for check existence"]
    ) -> typing.Annotated[bool, "define if flacour exis in database"]:
        return self.model.objects.filter(name=name).exists()

    def get_by_name_or_404(
        self, *, name: typing.Annotated[str, "name of flavour to retrieve"]
    ) -> typing.Annotated[Flavours | Http404, "404 error or flavour object with this name"]:
        return self.model.objects.get_object_or_404(name=name)

    def refill(self) -> None:
        self.quantity = 40
        self.save(update_fields=['quantity'])

    def refillable(self) -> list[Flavours]:
        return self.model.objects.filter(quantity=0).all()


class Flavours(models.Model):
    class Meta:
        verbose_name: typing.Annotated[str, "model name display on admin"] = 'flavour'
        verbose_name_plural: typing.Annotated[str, "model name display on admin for plurial"] = 'flavours'
        constraints: typing.Annotated[list[models.UniqueConstraint], "list of constraincts to reflect on database"] = [
            models.UniqueConstraint(
                fields=[
                    'name',
                ],
                name='unique name',
            ),
        ]

    name: typing.Annotated[models.CharField, "name of the flavour"] = models.CharField(max_length=30)
    quantity: typing.Annotated[
        models.PositiveIntegerField, "quantity available for the flavour"
    ] = models.PositiveIntegerField(default=40)
    price: typing.Annotated[models.PositiveIntegerField, "price for this flavour"] = models.PositiveIntegerField(
        default=2
    )

    @property
    def max_capacity(self) -> typing.Annotated[int, "max amount of ball per flavour"]:
        return 40

    def __str__(self) -> str:
        return self.name

    objects: typing.Annotated[FlavourManager, "collection of aditionnal request function"] = FlavourManager()
