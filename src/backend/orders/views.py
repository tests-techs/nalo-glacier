from __future__ import annotations

import logging
from http import HTTPStatus
from typing import TypedDict

from django.http import Http404
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse

from flavours.models import Flavours
from orders.models import OrderContent
from orders.models import Orders
from orders.serializers import OrdersSerializer

logger = logging.getLogger(__name__)


class OrderDict(TypedDict):
    prince: int
    identifier: str
    _links: dict[str, str]


class OrderDictError(TypedDict):
    error: str
    flavour: list[dict[str, str]]


class OrdersViewSet(viewsets.ViewSet):
    http_method_names: list[str] = ('get', 'post')
    permission_classes = (AllowAny,)

    def create(self, request) -> tuple[dict[str, str], int]:
        status_code: int
        data: dict[str, str | OrderDict | OrderDictError] = {'flavour': []}
        order: Orders = Orders()
        order_content: list[OrderContent] = []
        price: int = 0
        for flavour_slug, quantity in request.data.items():
            if quantity and Flavours.objects.exist_by_name(name=flavour_slug):
                flav = Flavours.objects.get(slug=flavour_slug)
                if quantity > flav.quantity:
                    logger.error(
                        "not enough %s for this order expected %s, available %s", flav.name, quantity, flav.quantity
                    )
                    print("send mail for not enougth %s", flav.name)
                    data['error'] = "no_enought_flavour"
                    data['flavour'].append(
                        {'name': flav.name, 'request_quantity': quantity, 'available_quantity': flav.quantity}
                    )
                    if not (flav.quantity - quantity):
                        logger.error("%s is empty", flav.name)
                        print("send mail for empty flavour %s", flav.name)
                else:
                    order_content.append(OrderContent(order=order, flavour=flav, quantity=quantity))
                    price += flav.price * quantity
            if price:
                order.total = price
                order.save()
                for content in order_content:
                    content.save()
                data['price'] = price
                data['identifier'] = order.identifier
                data['_links'] = {"retrieve": reverse('orders:order-retrieve', pk=order.identifier)}
                if 'error' in data.keys():
                    status_code = HTTPStatus.PARTIAL_CONTENT
                else:
                    status_code = HTTPStatus.OK
            else:
                data['error'] = "no_price"
                status_code = HTTPStatus.NO_CONTENT
        return Response(data=data, status=status_code)

    def retrieve(self, request, pk=None) -> tuple[dict[str, str], int]:
        data: dict[str, str] = {}
        try:
            order = Orders.objects.get_by_name_or_404(identifier=pk)
        except Http404:
            data['error'] = "invalid_identifier"
            status_code = HTTPStatus.NOT_FOUND
        else:
            data['order'] = OrdersSerializer(order).data
            status_code = HTTPStatus.OK
        return Response(data=data, status=status_code)
