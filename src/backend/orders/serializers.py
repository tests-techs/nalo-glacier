# Serializers define the API representation.
from rest_framework import serializers

from orders.models import Orders


class OrdersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Orders
        fields = ['creation_date', 'total', 'identifier']
