import random
import string
from http import HTTPStatus

from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from flavours.models import Flavours
from orders.models import Orders


class OrdersTestCase(TestCase):
    def setUp(self):  # pylint: disable=invalid-name
        self.client: APIClient = APIClient()
        self.order_create_url = 'order:order-create'
        self.order_retrieve_url = 'order:order-retrieve'

    def test_create_empty_status_code(self):
        response = self.client.post(reverse(self.order_create_url), data={})
        self.assertEqual(
            response.status_code,
            HTTPStatus.NO_CONTENT,
            msg=f"Expect {HTTPStatus.NO_CONTENT} NO CONTENT. got: {response.status_code}",
        )

    def test_create_valid(self):
        response = self.client.post(reverse(self.order_create_url), data={Flavours.objects.order_by('?')[0]: 2})
        self.assertEqual(
            response.status_code, HTTPStatus.OK, msg=f"Expect {HTTPStatus.OK} OK. got: {response.status_code}"
        )

    def test_create_price_calcul(self):
        response = self.client.post(
            reverse(self.order_create_url),
            data={Flavours.objects.order_by('?')[0]: 2, Flavours.objects.order_by('?')[0]: 1},
        )
        order = Orders.objects.latest('creation_date')
        self.assertEqual(response.json()['price'], 6, msg=f"Expect 6. got: {response.json()['price']}")

    def test_create_identifiers(self):
        response = self.client.post(
            reverse(self.order_create_url),
            data={Flavours.objects.order_by('?')[0]: 2, Flavours.objects.order_by('?')[0]: 1},
        )
        order = Orders.objects.latest('creation_date')
        self.assertIn('identifier', response.json().keys())
        self.assertEqual(
            response.json()['identifier'],
            order.identifier,
            msg=f"Expect {order.identifier}. got: {response.json()['identifier']}",
        )

    def test_create_links(self):
        response = self.client.post(
            reverse(self.order_create_url),
            data={Flavours.objects.order_by('?')[0]: 2, Flavours.objects.order_by('?')[0]: 1},
        )
        order = Orders.objects.latest('creation_date')
        self.assertEqual(response.json()['_links'], {"retrieve": reverse('orders:order-retrieve', pk=order.identifier)})

    def test_create_price_calcul_with_zero_quantity(self):
        response = self.client.post(reverse(self.order_create_url), data={Flavours.objects.order_by('?')[0]: 0})
        self.assertEqual(
            response.json()['error'], "no_price", msg=f"Expeceted no_price. got: {response.json()['error']}"
        )

    def test_create_price_calcul_with_over_quantity(self):
        flavour = Flavours.objects.order_by('?')[0]
        response = self.client.post(reverse(self.order_create_url), data={flavour.name: flavour.quantity + 1})
        self.assertEqual(
            response.status_code,
            HTTPStatus.PARTIAL_CONTENT,
            msg=f"Expect {HTTPStatus.PARTIAL_CONTENT} PARTIAL_CONTENT. got: {response.status_code}",
        )
        self.assertEqual(
            response.json()['error'],
            "no_enought_flavour",
            msg=f"Expeceted no_enought_flavour. got: {response.json()['error']}",
        )
        self.assertEqual(
            response.json()['flavour'],
            [{'name': flavour.name, 'request_quantity': flavour.quantity + 1, 'available_quantity': flavour.quantity}],
            msg=fr"Expeceted [\{'name': {flavour.name}, 'request_quantity': {flavour.quantity + 1}, 'available_quantity': {flavour.quantity}\}]. got: {response.json()['error']}",
        )

    def test_retrieve_unexisting_order(self):
        response = self.client.get(
            reverse(self.order_create_url, args=[''.join(random.choice(string.ascii_lowercase) for _ in range(10))])
        )
        self.assertEqual(
            response.json()['error'],
            "invalid_identifier",
            msg=f"Expeceted invalid_identifier. got: {response.json()['error']}",
        )

    def test_retrieve_order(self):
        flavour = Flavours.objects.order_by('?')[0]
        response_create = self.client.post(reverse(self.order_create_url), data={flavour.name: flavour.quantity})
        response = self.client.get(reverse(self.order_retrieve_url, args=[response_create.json()['identifier']]))
        self.assertEqual(
            response.status_code, HTTPStatus.OK, msg=f"Expect {HTTPStatus.OK} OK. got: {response.status_code}"
        )
