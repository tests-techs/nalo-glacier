import React from "react";
import axios from "axios";

export default class ApiHealthCheck extends React.Component {
    state = {
        message: "API is down"
    }

    componentDidMount() {
        console.log("url : htt:", process.env.REACT_APP_BACKEND_API + "health/");
        axios.get(process.env.REACT_APP_BACKEND_API + "health/", {
            headers: {"Access-Control-Allow-Origin": "*"},
        }).then(res => {
            console.log("res ", res);
            console.log("res data ", res.data);
            if ("message" in res.data){
                const message = "API is up";
                this.setState({message});
            }
        }).catch(error => {
            console.log(error);
            const message = "API encounter  an error, error : "+ error.toString();
            this.setState({message});
        })
    }

    render() {
        return <div>API is {this.state.message}</div>
    }
}
