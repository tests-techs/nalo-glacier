
<!-- toc -->

- [How To use](#how-to-use)
  - [Rregistry](#rregistry)
    - [URL](#url)
  - [Makefile](#makefile)
    - [Production mode](#production-mode)
      - [URL](#url-1)
    - [Development mode](#development-mode)
      - [URL](#url-2)
  - [Backend routes](#backend-routes)
  - [Account](#account)
- [Bugs](#bugs)
  - [Backend](#backend)
  - [Frontend](#frontend)
- [Evolutions](#evolutions)
  - [Backend](#backend-1)
  - [General](#general)
  - [Frontend](#frontend-1)
- [Makefile rules](#makefile-rules)

<!-- tocstop -->

------------------------------------------------------------------------------------

# How To use

## Rregistry

Actually don't work because CI pipeline don't work

`docker login registry.gitlab.com`

`curl https://gitlab.com/chrys40r/test-ci/-/blob/main/docker.env`
`curl https://gitlab.com/chrys40r/test-ci/-/blob/main/docker-compose-use.yaml`

`docker-compose --env-file docker.env --file docker-compose-use.yaml pull`
`docker-compose --env-file docker.env --file docker-compose-use.yaml up-detach`

### URL

backend: <http://127.0.0.1:8080>
frontend: <http://127.0.0.1:8888>

## Makefile

### Production mode

`make build` and `make up-detach service_name="backend-prod fronted-prod"`

#### URL

frontend: <http://127.0.0.1:8888>

### Development mode

`make build` and `make up-detach service_name="backend-dev fronted-dev"`

#### URL

backend: <http://127.0.0.1:8000>
frontend: <http://127.0.0.1:3000>

## Account

for connecting to the admin or use the refile root an account as been created  with migrations

- username: `nalo`
- password: `nalo`

## Backend routes

| Method    | URL                                | Description                | authentication  |
|-----------|------------------------------------|----------------------------|-----------------|
| `GET`     | /api/health/                       | check if API is UP         | no              |
| `GET`     | /api/flavour/                      | list of available flavours | no              |
| `GET`     | /api/flavour-refill/               | list refillable flavours   | yes             |
| `PATCH`   | /api/flavour-refill/<flavour-name> | refill flavour             | yes             |
| `GET`     | /api/order/<identifier>            | retrieve order             | no              |
| `POST`    | /api/order/                        | register order             | no              |

## creation Order Payload

```json
{
  *favour-name*: *quantity*
}
```

# Bugs

## Backend

- unit tests
- coverage
- statics prod

## Frontend

- Cors communication with backend

# Evolutions

## Backend

- API versionning
- use drf test framework instead of native django ? migrate to pytest ?
- test models/manager

## General

- test strategy
- split backend and frontend on separates repositories

## Frontend

- remove file outside build folder
- remove devDependencies

# Makefile rules

<!-- START makefile-doc -->
```
$ make help
make[1]: Entering directory '/mnt/d/onedrive/tests-techs/nalo'
Hello to the glacier Makefile

Variables:
	- "service_name" is a docker-compose service name or a list of services separate by space as string (database backend-dev backend-prod frontend-dev frontend-prod )


target                                             help                                                                             usage
------                                             ----                                                                             ----
backend-clean                                       remove useless files                                                             make backend-clean
backend-coverage                                    get backend test coverage                                                        make backend-coverage
backend-create-app                                  create django app                                                                make backend-create_app app_name#*app_name*
backend-database-migration                          apply database migrations                                                        make backend-database-migration
backend-documentation                               Build documentation                                                              make documentation
backend-mypy                                        Run mypy on code                                                                 make backend-mypy
backend-pylint                                      Run pylint on code                                                               make backend-pylint
backend-reports                                     Build coverage and pylint reports                                                make backend-reports
backend-syntax                                       launch backend syntax checker                                                   make backend-syntax
backend-syntax-reports                              check syntax                                                                     make backend-syntax-reports
backend-tests                                       Run tests for api                                                                make backend-tests
backend-upgradable-packages                         list outdated package in service                                                 make backend-upgradable-packages
backend-uwsgi-config                                display UWSGI config                                                             make backend-uwsgi-config
backend-uwsgi-logs                                  display UWSGI config                                                             make backend-uwsgi-logs
build-parallel                                      build containers in parallel                                                     make build-parallel [service_name#{service_name}]
build                                               build containers in parallel one by one                                          make build [service_name#{service_name}]
clean                                               build all documentation frontend                                                 make clean
config                                              display compiled docker-compose config                                           make config
connect                                             connect to container                                                             make connect [service_name#$service_name]
documentation                                       build all documentation                                                          make documentation
down                                                Down project containers                                                          make down
frontend-build                                      build frontend                                                                   make frontend-build
frontend-clean                                      remove useless files                                                             make frontend-clean
frontend-install-dependencies                       install package dependencies                                                     make frontend-install-dependencies
frontend-start                                      launch frontend                                                                  make frontend-start
frontend-syntax                                     launch syntax checker frontend                                                   make frontend-syntax
frontend-tests                                      launch tests frontend                                                            make frontend-tests
frontend-upgradable-packages                        list outdated package in services                                                make frontend-upgradable-packages
hadolint                                            lint dockerfiles                                                                 make hadolint
help                                                This help dialog.                                                                make help
logs-f                                              display logs with follow mode                                                    make logs-f [service_name#{service_name}]
logs                                                display logs                                                                     make logs [service_name#{service_name}]
pre-commit                                          run pre-commit pipe                                                              make pre-commit
prepare                                             prepare env file                                                                 make prepare
prune                                               remove service on the host and prune volume image and network unused             make prune
ps                                                  run docker-compose ps                                                            make ps
rebuild                                             Rebuild project containers                                                       make rebuild [service_name#{service_name}]
restart                                             Restart project containers                                                       make restart [service_name#{service_name}]
start                                               Start project containers                                                         make start [service_name#{service_name}]
stop                                                Stop project containers                                                          make stop [service_name#{service_name}]
syntax                                              global syntax checker                                                            make syntax
tests                                               launch test for back and front                                                   make tests
up-detach                                           up services                                                                      make up-detach [service_name#{service_name}]
upgradable-packages                                 list all package upgradable back and front                                       make upgradable-packages
up                                                  up services                                                                      make up [service_name#{service_name}]
make[1]: Leaving directory '/mnt/d/onedrive/tests-techs/nalo'
```
<!-- END makefile-doc -->
