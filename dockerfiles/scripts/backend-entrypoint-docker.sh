#!/bin/bash
set -e
BOOTSTRAPPED=/tmp/bootstrapped

wait-for-it  --timeout=0 ${DB_HOST}:${DB_PORT} --
if [ ! -e ${BOOTSTRAPPED} ]; then
    printf "Apply database migrations ...\n"
    python /app/manage.py migrate
    printf "create %s ...\n" ${BOOTSTRAPPED}
    touch ${BOOTSTRAPPED}
fi
exec "$@"
