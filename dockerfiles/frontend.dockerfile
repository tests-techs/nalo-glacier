FROM node:16.5.0-alpine3.11

ARG BACKEND_API

ENV REACT_APP_BACKEND_API=${BACKEND_API} \
    NODE_ENV=development \
    PATH=/app/node_modules/.bin:$PATH

WORKDIR /app

RUN set -x \
 && apk add --no-cache --update bash \
 && chown -R node:node /app \
 && rm -rf /var/cache/apk/*

USER node

COPY --chown=node:node src/frontend /app

RUN set -x \
 && npm update \
 && npm install --silent --quiet --no-audit --legacy-peer-deps \
 && npm cache clean --force \
 && npm run build \
 && mv /app/build /tmp/build \
 && rm -rf /app/* \
 && mv /tmp/build /app/build

ENV NODE_ENV=production

COPY --chown=node:node src/frontend/package.json /app/package.json

RUN npm install --silent --quiet --no-audit --legacy-peer-deps

CMD ["/app/node_modules/.bin/http-server","/app/build","-d=false","--log-ip","--gzip","--robots"]
